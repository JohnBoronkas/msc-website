<!doctype html>
<html class="no-js" lang="en-US"><head>
    <meta charset="utf-8">
    <title>My Smash Coach: Setting Goals</title>
    <meta name="description" content="My Smash Coach: Setting Goals">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="My Smash Coach: Setting Goals">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://mysmashcoach.com/learn/setting-goals/index.html">
    <meta property="og:image" content="/tile.png">

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="apple-touch-icon" href="/icon.png">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&amp;family=Signika+Negative&amp;family=Teko:wght@700&amp;display=swap" rel="stylesheet">

    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/main.css">

    <meta name="theme-color" content="#600099">
</head>

<body>
    <header>
        <div class="wrapper">
            <a href="/index.html" class="logo"><img src="/tile-wide.png" alt="My Smash Coach logo"></a>
            <input class="side-menu" type="checkbox" id="side-menu">
            <label class="hamburger" for="side-menu"><span class="hamburger-line"></span></label>
            <nav class="navigation">
                <ul>
                    <li><a href="/index.html"><div>About</div></a></li>
                    <hr>
                    <li><a href="/learn/index.html"><div>Learn</div></a></li>
                    <hr>
                    <li><a href="/privacy/index.html"><div>Privacy</div></a></li>
                </ul>
            </nav>
        </div>
    </header>

    <div class="wrapper">
        <div class="announcement nothidden">
            <p>This website is going offline. Join the new Discord here: <a href="https://discord.gg/rJrNrDjB48">https://discord.gg/rJrNrDjB48</a>!</p>
        </div>
    </div>

    <main class="wrapper">
        <article>
    <h1>Setting Goals</h1>
    <p>At this point, you hopefully have a <em>basic</em> <a href="/learn/end-state/index.html">end state</a> in mind. It doesn't need to be specific or anything, <em>yet</em>, but you should have an idea about how deep you want to go within Super Smash Bros. Ultimate or whatever skill you are looking to learn.</p>
    <p>I'm also assuming that you're just <em>slightly</em> ahead of <a href="/learn/square-one/index.html">square one</a>. Meaning you understand the basic flow of a match in Smash, you've played around for a bit online, and you've watched a few of the most basic YouTube videos that other content creators have put out there.</p>
    <p>So here we are. We <em>almost</em> have all of the pieces we need to design a "path" from A to B. You know where you want to go, you have some information surrounding the place you want to be, and now you just need the building blocks of how to actually get there. Which leads us to the most important building blocks in life for achieving anything -- <strong><em>goals</em></strong>.</p>
    <hr>
    <p>The important thing to remember is that <strong>no one else can decide on your end state or how to get there for you</strong>. It is on you, <em>yourself</em>, to create your own path and to get yourself to the destination that you set. Speaking from experience, the <em>first end state you achieve will be the hardest, but it does get easier the more you go though this process</em>.</p>
    <h2>What is a "goal" anyway?</h2>
    <p>A goal should be achievable within, <em>about</em>, a two week time frame. They should also be <em>measurable</em> and relatively easy to achieve. On the contrary, an end state can be <em>either</em> specific and measurable <em>or</em> floaty and vague. Having an end state that is just "do okay in tournaments and meet new people while having a good time" is a <em>perfectly valid</em> end state. However, <strong>goals <em>must</em> be extremely specific and measurable</strong>.</p>
    <p>There is some psychology behind why I put so much emphasis on this, but I won't bore you too much with it here. Basically, as you succeed more and more repeatedly, you start to have momentum behind you which lets you succeed even more in a positive feedback loop. Having a vague goal leaves you with a feeling of "I think I kinda mostly did the thing" instead of being able to point to something specific like "I just short hopped 50 times in a row".</p>
    <p>Some people will argue that end states should be specific too for a similar reason as above. But I vote that end states are just a false flag that we plant and the important part is the journey and process rather than the destination. Having and completing goals is the important part -- the end state is a way to link our goals together and give a rough projection of where we are headed.</p>
    <h2>So what's that "path" thing you were talking about?</h2>
    <p>A "path" is a special, hidden link between your goals that keeps you on track towards your end state. <strong>As you plan out your goals, you want to make logical jumps between them and make sure that they build off of each other</strong>.</p>
    <p>As an example, lets use our end state of being a top three player in a local tournament scene. Say we have a goal of "being able to do ten reverse aerial rushes in succession while moving to the left and right". A reverse aerial rush, or "RAR", is doing a series of back-air attack moves towards your opponent while keeping your momentum towards them at the same time.</p>
    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/WomJdf346po" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
    <p>This is a complicated input that requires multiple other skills before it is achievable. While you could start with this goal, it would be wiser to learn the individual inputs that make up the RAR and be able to do them independently as separate goals before attempting this RAR goal.</p>
    <p>So we could have a set of smaller goals that lead to this, such as:</p>
    <ol>
        <li>Be able to do 30 consecutive fast fall, short hops in a row</li>
        <li>Complete 20 consecutive fast fall, short hops while attacking and moving to the left and right</li>
        <li>Do 20 full hop RARs in a row, with a three second break between each one</li>
        <li>Do 14 fast fall, short hop RARs, with a two second break between each one</li>
        <li><strong>Do 10 fast fall, short hop RARs in succession while moving to the left and right</strong></li>
    </ol>
    <p>If you don't recognize the terms I'm using, I recommend that you google them, study for a little, and practice them for a minute or three to drive the concept home, even if you can't perform the inputs for them yet.</p>
    <p>Notice how our small set of goals creates a <em>path</em> that leads to our consecutive RAR goal. Each previous goal is a building block for the next. We nail down our fundamental short hop movements. Then we figure out the inputs required for a single RAR and, finally, chain everything together.</p>
    <h2>How many goals should make up my path?</h2>
    <p>This is the fun part. It really depends on you, your current skill level at whatever you're tying to do, and how much you know about it.</p>
    <p>It's okay to have a goal that is purely research based. Sometimes you don't know enough to build your path in the first place, so you need time to dig into the subject more to give yourself a better idea of where to head or how to get there. You need to know enough to know what you don't know.</p>
    <p>Your path to learn how to RAR may just be one or two goals long, but a different player may need as many as nine or ten to reach the same, or a similar, goal. While a third player may have no clue about how to get there, so they'll start with a researched based goal to help them find their own path.</p>
    <h3>How <em>many</em> goals should be in my initial path?</h3>
    <p>Your initial path to your end state should be more of a light sketch rather than a hard paved route to it. Your path will be extremely flexible and you should be reevaluating it after every two or three goals that you complete. This will help keep your path clean and it gives you the flexibility to add new goals as you learn new things and to readjust your path.</p>
    <p><em>I wouldn't recommend spending any more than 45 minutes to an hour on your initial path and no more than 15 to 30 minutes when reevaluating and readjusting your path after that</em>. This should be a fairly quick exercise to give you an idea of where you are headed and how to get there. Again, nothing you do should include just "going with the flow". <em>Set your own path and follow it</em>.</p>
    <p>Your initial path or paths should be somewhat linked, as shown in the RAR example. You can fill in the gaps and create new paths off of the main path as you complete your goals. Our example later on will better show how to have multiple paths where each individual path grows on itself, yet is separate from the other paths you have created.</p>
    <h2>How aggressive should my goals be?</h2>
    <p>This really depends on your end state and the time that you have available. If your end state is to hold your own in your local tournament scene, you'll probably have some pretty relaxed goals. Something like "land a down-air, off-stage, spike six times in a row, three on each side of the stage". If your end state is to be a nationally ranked player, then six times is a bit low. You'll want that number to be much higher, probably in the neighborhood of something like 60 times in a row.</p>
    <p>Again, this also goes off of how much <em>time</em> you have to dedicate to your goals. If you only have one hour a day to practice, a goal of 60 off-stage spikes in a row is extremely daunting and you don't have the time necessary to become a nationally ranked player. So you're better off setting your expectations accordingly by setting your sights much lower.</p>
    <p>Start with conservative goals and work your way up as you figure out what you are able, <em>and willing</em>, to achieve. If you want to be a nationally ranked player, have six hours a day to play, but frequently use four of those hours to do something else, then you <em>really</em> only have two hours a day if you stick with it.</p>
    <p>If it isn't a real priority in your life, you won't achieve what you set out to do. This is part of the reason why setting a real, <em>serious</em>, <a href="/learn/end-state/index.html">end state</a> is <em>extremely</em> important for any of this to work for you.</p>
    <h3>But I already set a goal that's too high for me, ): help!</h3>
    <p>That's okay! Make your path <em>to</em> that goal include smaller versions of that goal. So if your original goal was to do 50 short hops in a row and you can only do 20 or so. Make a new goal for doing 30 in a row, then another to do 40 in a row and slowly work your way up instead of giving up and scraping the original high goal that you set for yourself.</p>
    <p><strong>Get in the habit of completing anything that you promise yourself that you'll do — no exceptions allowed.</strong></p>
    <h2>What's a "recurring goal"?</h2>
    <p>A recurring goal is simply a goal that can be performed multiple times, back-to-back. This is just another tool that you can use when creating a path to help reinforce previously learned concepts.</p>
    <p>This is easiest to explain with an example. Say your goal was to do 30 fast fall, short hops in a row. Once you hit that number, you may want to make a new goal of "every day for two weeks, I will do 10 fast fall, short hops in a row". This essentially turns your goal into a habit that you do every day.</p>
    <p>You can swap recurring goals out for each other to help keep a variety of skills up to par, but your daily practice mode time will be the primary way for you to keep your newly found skills sharp.</p>
    <p>Recurring goals are for reinforcing newly learned things before putting them on the shelf, tucked away in your daily practice routines where you otherwise wouldn't spend as much time on them. If you notice that one of your previous skills is slacking, you can turn it into a recurring goal so that you give your skill the time it needs to get back up to par before putting it on the back-burner again.</p>
    <h2>How many goals should I be working towards at the same time?</h2>
    <p>Ideally, you would only work one goal at a time. But this is horribly inefficient and we can do much better. The <em>trick</em> is to work on two or three <em>different</em> types of goals at the same time. If you include recurring goals, this number could go as high as four or five — but that is pushing the limits of spreading yourself too thin.</p>
    <p>By different <em>types</em> of goals, I mean goals that are disjointed. As an example, if I wanted to make my local English Football, or American Soccer, team I would generally have three <em>types</em> of goals. Running related, weight lifting related, and football skill related. I can focus on running for a set amount of time without being fatigued, working on core exorcises, and hitting the top-corner of the net with my shots. Notice how these all focus on different areas and do not conflict with each other. I wouldn't advise trying to run for a set distance <em>and</em> working on my sprint times since they do conflict with each other and working towards one goal directly affects my progress towards the other goal in a negative way.</p>
    <p>In Smash talk, it doesn't makes sense to work on a complicated input such as a RAR while also trying to work on attack canceling. While trying to work on the muscle memory required to consistently input these precise moves, working on one will confuse your body and actually make it harder -- thus increasing the time it takes to achieve your goals.</p>
    <h2>What if I fail to meet my goal?</h2>
    <p>Failure is a part of the process and there are no time limits for reaching your goals. The more you fail, the sweeter the achievement will taste in the end. There is a <strong>big</strong> difference between a failure and a <em>defeat</em>. <em>A failure is an experience that you learn from</em>. A defeat is when you give up and throw in the towel. You get nothing from defeats and the forward momentum, or positive feedback loop, I was talking about before can just as easily go in the other direction. <strong>One defeat can easily set-off a chain of defeats</strong>.</p>
    <p>Think about going to the gym or trying to run everyday or even following a diet. That <em>one</em> cheat day makes the next day <em>that much harder</em> to get back into it. So don't lie to yourself. If you commit to do something, never give up and find a way to complete it. You have to make it a priority to complete the things that you tell yourself you will do.</p>
    <p>This is why we try to set smaller goals that we know we'll be able to achieve in a small-ish amount of time. This way we can trick our minds and get the momentum to help us, rather than for it to be against us.</p>
    <p>If you need to, it's okay to adjust your path by adding new goals to it to help achieve a difficult goal, but <strong><em>do not give up</em></strong>. Let the goal sit there and be your new bar. Do what it takes to achieve it and reward yourself for it once you to complete it.</p>
    <h3>Reward myself?</h3>
    <p>As you complete goals, reward yourself with something that isn't directly at odds with your goal. For instance, if you met a goal of losing 10 pounds you should probably stay away from using sugar or food as a reward. Instead, buy yourself a piece of new clothing that you feel good in or let yourself watch that new movie that you've been dying to see or buy that new game that came out last week.</p>
    <p>This is a great way to help enforce that momentum and positive feedback loop that we were talking about near the beginning of this post.</p>
    <h2>So about that example...</h2>
    <p>Right! So now you have all the information you need to make your <em>own</em> path. Let me share my hypothetical path, as I promised before, as an example before I kick you off the cliff and tell you to go do it.</p>
    <p>Again, I'm making a path for a hypothetical end state of being a top three player in my local tournament scene.</p>
    <p>First, I need to find out more about the local tournament scene in my area. So I should find out about the different tournaments that are held, where they are, how to attend, how many people go to them, how much do they cost, how often are they held, and so on. A reasonable goal for this would be "find and document at least three different local tournaments and attend at least one of them".</p>
    <p>Now that I know where my tournament scene is, I should try to make some friends from there. I want to know who the big players are, find their replays, their blogs, YouTube videos, and anything else I can find that relates to their Smash playing skills. This way I can gauge my competition for that top three spot. So my goal could be "find the top five players in the tournament scene that I attended, get their online names, and research each one to find out how good they are. Bonus points if I can find replays of them playing to study in a future goal".</p>
    <p>And this is where we can start to dive into my in-game aspects since I know how to go to those tournaments and I know what level my competition is at. If my competition turned out to be regional or national ranked players, I may want to switch to an easier local tournament scene to build up my skills before facing off against them.</p>
    <p>Since I'm already a fairly skilled player, I can skip over some of the goals that you may need to go through first, like that short hop example I used before. I know that, as a Robin player, I need to work on catching my Sword and Tomes once they expire. So my personal goal is to "be able to catch my expended resource 14 times in a row, seven while facing left and seven while facing right".</p>
    <p>To continue to add to my skill set, I know that I need to get a better handle on Robin's kill confirms. Making my fourth goal "perform the top two kill confirms from each leading move, three times each while facing both left and right — in a row".</p>
    <p>Now we have two research based goals and two in-game goals. Let's say we get anxiety while playing in tournaments. To help with this a goal could be to "participate in every <a href="https://discord.gg/crazyhand" target="_blank">Crazy Hand Tournament</a> for a month". This will give us four weekly tournaments and one monthly tournament. It's double elimination, so we'll get at least two games out of each one to help us control our emotions and start to get used to the feeling of competition.</p>
    <p>Notice how our goals are sorta in parallel, as long as we don't work the first two research goals or the second two in-game goals at the same time then they won't be conflicting with each other. That lets us work three of our goals at the same time.</p>
    <p>As time goes on and we complete those first three goals, we can expand our paths. As an example, I may want to learn how to z-drop my expired resources after I catch them and then learn some of the item combos that Robin can do.</p>
    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/4A-HZvEJcfo?start=119" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
    <p>The video is from Smash 4, but the same concepts apply to Smash Ultimate.</p>
    <h2>That's it!</h2>
    <p><strong><em>Now go and make your own path</em></strong>. It will take time, so block out some time for yourself to dedicate to it. And by yourself I mean all of you -- <em>you aren't alone with this</em>. Join <a href="https://discord.gg/xBw7EWP" target="_blank">our Discord</a> and ask the community for help!</p>
    <p>Finally, don't be afraid to send me your path on Discord. I'd love to see how many of you are crazy enough to follow though and create a plan for yourself. And the more I see, the easier it'll be for me to help guide others or to put together a FAQ for designing a path.</p>
    <p>Thanks for sticking around and good luck!</p>
</article>
    </main>

    <footer>
        <div class="wrapper">
            <p class="copyright">© 2022 My Smash Coach - All rights reserved.</p>
            <article>
                <h3>Social</h3>
                <nav>
                    <ul>
                        <li><a href="https://discord.gg/xBw7EWP" target="_blank">Discord</a></li>
                        <li><a href="https://www.patreon.com/mysmashcoach?fan_landing=true" target="_blank">Patreon</a></li>
                        <li><a href="https://twitter.com/MySmashCoach" target="_blank">Twitter</a></li>
                        <li><a href="https://www.youtube.com/channel/UC3MJNCIXDermcReBqFL0EDA" target="_blank">YouTube</a></li>
                    </ul>
                </nav>
            </article>
            <article>
                <h3>Links</h3>
                <nav>
                    <ul>
                        <li><a href="/index.html"><div>About</div></a></li>
                        <li><a href="/learn/index.html"><div>Learn</div></a></li>
                        <li><a href="/privacy/index.html"><div>Privacy</div></a></li>
                    </ul>
                </nav>
            </article>
            <div class="clearfix"></div>
        </div>
    </footer>

    <script type="module" src="/js/vendor/modernizr-3.11.2.min.js"></script>
    <script type="module" src="/js/plugins.js"></script>
    <script type="module" src="/js/main.js"></script>


</body></html>