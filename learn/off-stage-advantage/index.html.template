<title>Off-stage Advantage</title>

<content>
    <h1>Off-stage Advantage</h1>
    <p>For daily practice today/tomorrow we're kicking it off easy! Remember to pick one character and use that character for the entire lesson. This is why picking a main character is so important. The theme is <strong>off-stage advantage</strong> (estimated ~15 minutes, but feel free to extend it to 30 minutes!).</p>
    <p>Being comfortable off-stage is extremely important and is a key way to press an advantage and get an early kill.</p>
    <p>In practice mode on your tourny-stage of choice, jump off of the left side of the stage and recover back to the ledge. Try to find the limit of how far off-stage you can get with your character while still making it back. If you don't SD at some point, then you're doing it wrong! Try jumping off at different angles. How far horizontally can you go? How far vertically can you go? What about the random angles in between? <strong>You need to be comfortable going off-stage with your character!</strong></p>
    <p>After doing this for 1-2 minutes, switch to the right side of the stage and do the same thing again for another 1-2 minutes.</p>
    <p>Now we're going to mix-in some off-stage options. Go back and do the same as above, but try to mix-in different types of attacks. Picture an opponent that is trying to recover and put-up hitboxes where you think they would go, then get back to the stage. The limit of how far you can go should be a little shorter, so re-learn your limits and get comfortable tossing out attacks while off-stage and riding that limit.</p>
    <p>Do this for 2-3 minutes per side of the stage.</p>
    <p>For this final section, we'll be using, what I call, <strong>"Lab"</strong> settings which is unlimited time, unlimited stock, the score counter on, and is otherwise normal competitive settings.</p>
    <p>Setup a game with the Lab settings, but set the time limit to 3 minutes. And make three level 3 CPUs in free-for-all. Your goal is to knock the CPU's off-stage, then either spike or gimp them using off-stage attacks as many times as possible in the 3 minutes. <em>It's expected that you'll SD, so don't get discouraged! That's the point of practicing!</em></p>
    <p>After the match, give yourself a minute to reflect on the match and think about how it went and what you can do better. <strong>Write at least 1 thing down that you want to improve on based on that match.</strong> Then go back in for another 3 minute match and focus on what you wrote down and try to improve your score.</p>
    <p><strong>Post your high scores to our <a href="https://discord.gg/xBw7EWP" target="_blank">Discord</a>! Remember that only off-stage kills count!</strong></p>
</content>
