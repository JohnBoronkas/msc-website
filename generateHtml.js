"use strict";

const { readFileSync, writeFileSync } = require('fs');
const glob = require('glob');
const { JSDOM } = require('jsdom');

const packageJson = JSON.parse(readFileSync('package.json', 'utf8', (err, data) => {
    if (err) {
        return console.error(err);
    }

    return data;
}));

// TODO Move this template into another file
// TODO Create a sitemap: https://www.sitemaps.org/protocol.html
// TODO On .html.template or generateHTML.js edit, regenerate only the relevant file(s)
const generatedContentMissing = "Error: This field was supposed to be set by generateHtml.js, but it wasn't. Check the script's generateHtml function."
const baseHtmlTemplate = `
<!doctype html>
<html class="no-js" lang="en-US">

<head>
    <meta charset="utf-8">
    <title>${generatedContentMissing}</title>
    <meta name="description" content="${generatedContentMissing}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="${generatedContentMissing}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="${generatedContentMissing}">
    <meta property="og:image" content="/tile.png">

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <link rel="manifest" href="/site.webmanifest">
    <link rel="apple-touch-icon" href="/icon.png">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&family=Signika+Negative&family=Teko:wght@700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/main.css">

    <meta name="theme-color" content="${packageJson.websiteThemeColor}">
</head>

<body>
    <header>
        <div class="wrapper">
            <a href="/index.html" class="logo"><img src="/tile-wide.png" alt="${packageJson.websiteName} logo"/></a>
            <input class="side-menu" type="checkbox" id="side-menu"/>
            <label class="hamburger" for="side-menu"><span class="hamburger-line"></span></label>
            <nav class="navigation">
                <ul>
                    <li><a href="/index.html"><div>About</div></a></li>
                    <hr/>
                    <li><a href="/learn/index.html"><div>Learn</div></a></li>
                    <hr/>
                    <li><a href="/privacy/index.html"><div>Privacy</div></a></li>
                </ul>
            </nav>
        </div>
    </header>

    <div class="wrapper">
        <div class="announcement nothidden">
            <p>This website is going offline. Join the new Discord here: <a href="https://discord.gg/rJrNrDjB48">https://discord.gg/rJrNrDjB48</a>!</p>
        </div>
    </div>

    <main class="wrapper">
        <article>${generatedContentMissing}</article>
    </main>

    <footer>
        <div class="wrapper">
            <p class="copyright">© ${new Date().getFullYear()} ${packageJson.websiteName} - All rights reserved.</p>
            <article>
                <h3>Social</h3>
                <nav>
                    <ul>
                        <li><a href="https://discord.gg/xBw7EWP" target="_blank">Discord</a></li>
                        <li><a href="https://www.patreon.com/mysmashcoach?fan_landing=true" target="_blank">Patreon</a></li>
                        <li><a href="https://twitter.com/MySmashCoach" target="_blank">Twitter</a></li>
                        <li><a href="https://www.youtube.com/channel/UC3MJNCIXDermcReBqFL0EDA" target="_blank">YouTube</a></li>
                    </ul>
                </nav>
            </article>
            <article>
                <h3>Links</h3>
                <nav>
                    <ul>
                        <li><a href="/index.html"><div>About</div></a></li>
                        <li><a href="/learn/index.html"><div>Learn</div></a></li>
                        <li><a href="/privacy/index.html"><div>Privacy</div></a></li>
                    </ul>
                </nav>
            </article>
            <div class="clearfix"></div>
        </div>
    </footer>

    <script type="module" src="/js/vendor/modernizr-3.11.2.min.js"></script>
    <script type="module" src="/js/plugins.js"></script>
    <script type="module" src="/js/main.js"></script>
</body>
</html>
`;

function generateHtml(htmlTemplateFilePath) {
    console.log(`Generating HTML for ${htmlTemplateFilePath}`);

    const htmlTemplate = readFileSync(htmlTemplateFilePath, 'utf8', (err, data) => {
        if (err) {
            console.error(err);
            return;
        }

        return data;
    });

    const pageSlug = htmlTemplateFilePath.replace(__dirname.replace(/\\/g, '/'), '').replace('.template', '');

    const templateDom = new JSDOM(htmlTemplate);
    const templateTitle = templateDom.window.document.querySelector('title').textContent;
    const templateContent = templateDom.window.document.querySelector('content').innerHTML;

    const baseDom = new JSDOM(baseHtmlTemplate);
    const websiteNameDesc = `${packageJson.websiteName}: ${templateTitle}`;
    baseDom.window.document.querySelector('head title').textContent = websiteNameDesc;
    // TODO Add og:description that is relevant for each page https://stackoverflow.com/questions/38173163/seo-meta-tag-ogdescription-vs-description
    baseDom.window.document.querySelector('head meta[name="description"]').setAttribute('content', websiteNameDesc);
    baseDom.window.document.querySelector('head meta[property="og:title"]').setAttribute('content', websiteNameDesc);
    baseDom.window.document.querySelector('head meta[property="og:url"]').setAttribute('content', packageJson.websiteUrl + pageSlug);
    baseDom.window.document.querySelector('body article').innerHTML = templateContent;

    if (baseDom.window.document.querySelector('html').innerHTML.includes(generatedContentMissing)) {
        console.error(`Generated content missing in ${htmlTemplateFilePath}: ${baseDom.window.document.querySelector('html').innerHTML}`);
        return;
    }

    writeFileSync(`${htmlTemplateFilePath.replace('.template', '')}`, '<!doctype html>\n' + baseDom.window.document.documentElement.outerHTML, 'utf8', (err) => {
        if (err) {
            console.error(err);
            return;
        }
    });
}

glob(__dirname + '/**/*.html.template', {}, (err, files) => {
    if (err) {
        console.error(err);
        return;
    }

    for (const file of files) {
        generateHtml(file);
    }
});
